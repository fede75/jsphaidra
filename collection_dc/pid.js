

    function loadPids() {
	    var idCollection = $("#selCollezione").val();
	    $.get("collectionmembers2.php?collezione="+idCollection, function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
		
		
			var obj = JSON.parse(data);
			var members = obj.metadata.members;
			var listaForSelect =[];
			for (var i =0; i < members.length; i++) {
				listaForSelect.push(members[i].pid);
			}
						
			$('#selMembri')
				.find('option')
				.remove()
				.end();
				
			$('#selMembri').append($('<option>', { 
					value: '',
					text : 'Seleziona....'
				}));	
				
			$.each(listaForSelect, function (i, item) {
				$('#selMembri').append($('<option>', { 
					value: item,
					text : item
				}));
						
			});
		
		
		

	    
		});



}


$(document).ready(function(){
	$('#selMembri').on('change', function()
	{

		 var selectedMember = $('#selMembri').val();
		 $("#dc").empty();
		 if (!selectedMember || selectedMember==='') return;
		 			

		 var url = "https://phaidradev.cab.unipd.it/api/object/"+selectedMember+"/dc";
		 $.getJSON(url, function(response){
			console.log(response.metadata.dc);
			
			//https://phaidradev.cab.unipd.it/api/object/o:"+testo+"/related?fields=dc.description&from=1&limit=1000&relation=info:fedora%2Ffedora-system:def%2Frelations-external%23hasCollectionMember
			
			
			$("#dc").append('<div><img src="https://fc.cab.unipd.it/fedora/objects/'+selectedMember+'/methods/bdef:Asset/getThumbnail"></img></div><hr>');

			$.each(response.metadata.dc, function(i, field){
				console.log(field);
				$("#dc").append(field.xmlname + ": " + field.ui_value + "<br />");

			});
		});
		
		
	     var urlRelated = "https://phaidradev.cab.unipd.it/api/object/"+selectedMember+"/related?fields=dc.description&from=1&limit=1000&relation=info:fedora%2Ffedora-system:def%2Frelations-external%23hasCollectionMember";
		 $.getJSON(urlRelated, function(response){
			console.log(response);
			
			//https://phaidradev.cab.unipd.it/api/object/o:"+testo+"/related?fields=dc.description&from=1&limit=1000&relation=info:fedora%2Ffedora-system:def%2Frelations-external%23hasCollectionMember
			
			
			$("#relatedContainer").text(JSON.stringify(response, null, 2));
			//append('<div><img src="https://fc.cab.unipd.it/fedora/objects/'+selectedMember+'/methods/bdef:Asset/getThumbnail"></img></div><hr>');

			/*.each(response.metadata.dc, function(i, field){
				console.log(field);
				$("#dc").append(field.xmlname + ": " + field.ui_value + "<br />");

			});*/
		});
	});
 });




	  
	  




